﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using HealthyPet.DataAccess;
using HealthyPet.Entities;
using HealthyPet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace HealthyPet.Controllers
{
    [Route("api/[controller]")]
    public class PetsController : Controller
    {
        private readonly AppDbContext _db;
        private readonly IConfiguration _config;

        public PetsController(AppDbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        [HttpGet("{id}")]
        public IEnumerable<Pet> GetByUserId(Guid id)
        {
            return _db.Users.Include(x => x.Pets).FirstOrDefault(x => x.Id == id).Pets;
        }

        [HttpGet("{id}/image")]
        public ActionResult GetImage(Guid id)
        {
            var pet = _db.Pets.FirstOrDefault(x => x.Id == id);

            var image = System.IO.File.OpenRead(pet.PathToImage);
            return File(image, "image/jpeg");
        }


        [HttpPost]
        public IEnumerable<Pet> Create([FromForm]PetCreateModel model)
        {
            var filename = $"{model.Name}-img-{DateTime.Now.Ticks}.";
            var path = String.Empty;

            using (var memoryStream = new MemoryStream())
            {
                model.Image.CopyTo(memoryStream);

                using (var im = Image.FromStream(memoryStream))
                {


                    ImageFormat frmt;

                    if (ImageFormat.Png.Equals(im.RawFormat))
                    {
                        filename += "png";
                        frmt = ImageFormat.Png;
                    }
                    else
                    {
                        filename += "jpg";
                        frmt = ImageFormat.Jpeg;
                    }

                    path = _config.GetSection("Consts")["PathToStorage"] + filename;

                    im.Save(path, frmt);
                }
            }

            var user = _db.Users.FirstOrDefault(x => x.Id == model.UserId);

            user.Pets = new List<Pet>
            {
                new Pet { Age = model.Age, Name = model.Name, PathToImage = path }
            };

            _db.SaveChanges();

            return _db.Users.FirstOrDefault(x => x.Id == model.UserId).Pets;
        }
    }
}
