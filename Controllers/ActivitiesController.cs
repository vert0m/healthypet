﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using HealthyPet.DataAccess;
using HealthyPet.Entities;
using HealthyPet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Net.Mail;
using System.Net;

namespace HealthyPet.Controllers
{
    [Route("api/[controller]")]
    public class ActivitiesController : Controller
    {
        private readonly AppDbContext _db;
        private readonly IConfiguration _config;

        public ActivitiesController(AppDbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        [HttpGet("send-email")]
        public void SendEmail()
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("vlad.ostapenko.ua@gmail.com", "273581Timka")
            };

            using (var message = new MailMessage("vlad.ostapenko.ua@gmail.com", "vlad.ostapenko.ua@gmail.com")
            {
                Subject = "Subject",
                Body = "vscvsdvsfvvevfe"
            })
            {
                smtpClient.Send(message);
            }
        }

        [HttpGet("admin")]
        public IEnumerable<Activity> GetActivitiesForAdmin()
        {
            return _db.Activities.Include(x => x.ActivityType).Include(x => x.Pet);
        }

        [HttpPost]
        public IEnumerable<Activity> Get([FromBody]ActivityByDateModel model)
        {
            var startDate = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, 0, 0, 0);
            var endDate = startDate.AddHours(24);
            return _db.Activities.Include(x => x.ActivityType).Where(x => x.Pet.Id == model.PetId && x.StartDate >= startDate && x.StartDate <= endDate);
        }

        [HttpGet("{petId}")]
        public IEnumerable<Activity> Get(Guid petId)
        {
            return _db.Activities.Include(x => x.ActivityType).Where(x => x.Pet.Id == petId);
        }

        [HttpGet("types")]
        public IEnumerable<ActivityType> GetTypes(Guid petId)
        {
            return _db.ActivityTypes;
        }

        [HttpPost("{petId}")]
        public IEnumerable<Activity> Create([FromBody]Activity model, Guid petId)
        {
            var pet = _db.Pets.FirstOrDefault(x => x.Id == petId);
            var activityType = _db.ActivityTypes.FirstOrDefault(x => x.Id == model.ActivityType.Id);

            pet.Activity.Add(new Activity
            {
                ActivityType = activityType,
                Duration = model.Duration,
                StartDate = model.StartDate,
                Description = model.Description
            });

            _db.SaveChanges();

            return _db.Pets.Include(x => x.Activity).FirstOrDefault(x => x.Id == petId).Activity;
        }
    }
}
