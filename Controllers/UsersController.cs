﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthyPet.DataAccess;
using HealthyPet.Entities;
using HealthyPet.Models;
using Microsoft.AspNetCore.Mvc;

namespace HealthyPet.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly AppDbContext _db;

        public UsersController(AppDbContext db)
        {
            _db = db;
        }

        // SIGN IN
        [HttpPost("sign-in")]
        public IActionResult SignIn([FromBody]SignInModel model)
        {
            var user = _db.Users.FirstOrDefault(x => x.Email == model.Email &&
                x.Password == model.Password);

            if (user != null)
            {
                return Ok(user);
            }

            return NotFound();
        }

        // SIGN UP
        [HttpPost("sign-up")]
        public IActionResult SignUp([FromBody]SignInModel model)
        {
            var user = _db.Users.Add(new User
            {
                Email = model.Email,
                Password = model.Password
            });

            _db.SaveChanges();

            return Ok(user.Entity);
        }
    }
}
