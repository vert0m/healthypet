﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthyPet.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivityTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PetTypesUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PetTypesUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PetTypeId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    PathToImage = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pets_PetTypesUsers_PetTypeId",
                        column: x => x.PetTypeId,
                        principalTable: "PetTypesUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pets_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ActivityTypeId = table.Column<Guid>(nullable: true),
                    PetId = table.Column<Guid>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Activities_ActivityTypes_ActivityTypeId",
                        column: x => x.ActivityTypeId,
                        principalTable: "ActivityTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Activities_Pets_PetId",
                        column: x => x.PetId,
                        principalTable: "Pets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ActivityTypes",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("6c2d1ff1-3cda-43cf-92cc-d2a40731a319"), "Feed" });

            migrationBuilder.InsertData(
                table: "ActivityTypes",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("af354952-c176-43f3-b31d-63f30cf7056b"), "Walk" });

            migrationBuilder.InsertData(
                table: "ActivityTypes",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("9505d9d7-09b7-4977-8cac-0cd0e74a0a78"), "Sleep" });

            migrationBuilder.InsertData(
                table: "ActivityTypes",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("6e8ae65b-0c2d-4084-8ae1-2055b22b3b69"), "Treatment" });

            migrationBuilder.InsertData(
                table: "ActivityTypes",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("3f0b0afd-56ba-4de4-b0c2-b08fc47f0394"), "Visit vet" });

            migrationBuilder.InsertData(
                table: "PetTypesUsers",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("8863f66d-d4b0-4945-be1f-982a1361ce71"), "Dog" });

            migrationBuilder.InsertData(
                table: "PetTypesUsers",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("ad538eab-c7d6-48ae-b131-6e437c2dbce5"), "Cat" });

            migrationBuilder.InsertData(
                table: "PetTypesUsers",
                columns: new[] { "Id", "Value" },
                values: new object[] { new Guid("a4d74970-85b3-43cf-bc85-5b394a256c4d"), "Parrot" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "Password", "Role" },
                values: new object[] { new Guid("b00cee73-3958-4b37-9644-a1042fdcfcd7"), "admin@admin", "admin", 0 });

            migrationBuilder.CreateIndex(
                name: "IX_Activities_ActivityTypeId",
                table: "Activities",
                column: "ActivityTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Activities_PetId",
                table: "Activities",
                column: "PetId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_PetTypeId",
                table: "Pets",
                column: "PetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_UserId",
                table: "Pets",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "ActivityTypes");

            migrationBuilder.DropTable(
                name: "Pets");

            migrationBuilder.DropTable(
                name: "PetTypesUsers");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
