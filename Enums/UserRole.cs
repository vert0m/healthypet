﻿using System;

namespace HealthyPet.Entities
{
    public enum UserRole
    {
        Admin,
        User
    }
}
