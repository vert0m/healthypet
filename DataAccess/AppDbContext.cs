﻿using System;
using HealthyPet.Entities;
using Microsoft.EntityFrameworkCore;

namespace HealthyPet.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { Id = Guid.NewGuid(), Email = "admin@admin", Password = "admin", Role = UserRole.Admin });

            modelBuilder.Entity<ActivityType>().HasData(
                new ActivityType { Id = Guid.NewGuid(), Value = "Feed" },
                new ActivityType { Id = Guid.NewGuid(), Value = "Walk" },
                new ActivityType { Id = Guid.NewGuid(), Value = "Sleep" },
                new ActivityType { Id = Guid.NewGuid(), Value = "Treatment" },
                new ActivityType { Id = Guid.NewGuid(), Value = "Visit vet" });

            modelBuilder.Entity<PetType>().HasData(
                new PetType { Id = Guid.NewGuid(), Value = "Dog" },
                new PetType { Id = Guid.NewGuid(), Value = "Cat" },
                new PetType { Id = Guid.NewGuid(), Value = "Parrot" });
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<PetType> PetTypesUsers { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityType> ActivityTypes { get; set; }
    }
}
