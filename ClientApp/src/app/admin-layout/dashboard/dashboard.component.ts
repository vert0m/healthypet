import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityService } from '../../services/activity.service';
import { Activity } from '../../models/activity';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  model: Activity[];

  constructor(private router: Router,
    private activityService: ActivityService) { }

  ngOnInit() {
    this.activityService.getForAdmin()
      .subscribe(activities => {
        this.model = activities;
        console.log(this.model);
      });
  }

  logOut(): void {
    this.router.navigate(['']);
    // todo
  }

  sendEmail(activity: Activity): void {
    console.log(activity);
  }
}
