import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PetService } from 'src/app/services/pet.service';
import { PetCreateModel, Pet } from 'src/app/models/pet';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.scss']
})
export class AddModalComponent implements OnInit {
  @Input() id;
  @ViewChild('fileInput') fileInput;
  model: PetCreateModel;

  constructor(public activeModal: NgbActiveModal,
              private petService: PetService) {
    this.model = new PetCreateModel();
  }

  ngOnInit() {
  }

  addPet(): void {
    const fileBrowser = this.fileInput.nativeElement;

    this.model.userId = this.id;

    if (fileBrowser.files && fileBrowser.files[0]) {
      this.petService.addPet(fileBrowser.files[0], this.model).subscribe(() => {
        this.activeModal.close();
      });
    } else {
      this.petService.addPet(null, this.model).subscribe(() => {
        this.activeModal.close();
      });
    }
  }
}
