import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbCalendar, NgbDateAdapter, NgbDateNativeAdapter, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ActivityService } from '../../../services/activity.service';
import { ActivityType, Activity } from '../../../models/activity';

@Component({
  selector: 'app-add-activity-modal',
  templateUrl: './add-activity-modal.component.html',
  styleUrls: ['./add-activity-modal.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }, NgbDropdownConfig]
})
export class AddActivityModalComponent implements OnInit {
  @Input() id;
  date: Date;
  activityTypes: ActivityType[];
  model: Activity;

  constructor(public activeModal: NgbActiveModal,
              calendar: NgbCalendar,
              public activityService: ActivityService,
              public dconfig: NgbDropdownConfig) {
    this.model = new Activity();
    this.model.startDate = new Date();
  }

  ngOnInit() {
    this.activityService.getTypes()
      .subscribe(types => this.activityTypes = types);
  }

  onChangeActivityType(value: string): void {
    this.model.activityType = this.activityTypes.find(x => x.value === value);
  }

  add(): void {
    console.log(this.model);
    this.activityService.add(this.model, this.id)
      .subscribe(response => {
        this.activeModal.close();

      });
  }
}
