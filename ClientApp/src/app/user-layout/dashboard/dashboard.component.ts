import { Component, OnInit } from '@angular/core';
import { AddModalComponent } from './add-modal/add-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { InfoModalComponent } from './info-modal/info-modal.component';
import { PetService } from 'src/app/services/pet.service';
import { Pet } from 'src/app/models/pet';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userId: string;
  pets: Pet[];

  constructor(private modalService: NgbModal,
              private router: Router,
              private petService: PetService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['id'];
      this.petService.getByUserId(this.userId).subscribe(pets => { this.pets = pets; console.log(pets); });
    });
  }

  addPet(): void {
    const addModal = this.modalService.open(AddModalComponent);
    addModal.componentInstance.id = this.userId;
    addModal.result.then(() => this.petService.getByUserId(this.userId).subscribe(pets => this.pets = pets));
  }

  logOut(): void {
    this.router.navigate(['']);
    // todo
  }

  openInfoModal(pet: any): void {
    const infoModal = this.modalService.open(InfoModalComponent, { centered: true });
    infoModal.componentInstance.pet = pet;
  }
}
