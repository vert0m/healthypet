import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbCalendar, NgbDateAdapter, NgbDateNativeAdapter, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddActivityModalComponent } from '../add-activity-modal/add-activity-modal.component';
import { Pet } from '../../../models/pet';
import { Activity, ActivityByDateModel } from '../../../models/activity';
import { ActivityService } from '../../../services/activity.service';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.scss'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})
export class InfoModalComponent implements OnInit {
  @Input() pet: Pet;
  date: Date;
  model: Activity[];
  requestModel: ActivityByDateModel;

  constructor(public activeModal: NgbActiveModal,
              calendar: NgbCalendar,
              private modalService: NgbModal,
              private activityService: ActivityService) {
    this.date = new Date();
    this.requestModel = new ActivityByDateModel();
  }

  ngOnInit() {
    this.requestModel.date = this.date;
    this.requestModel.petId = this.pet.id;

    this.activityService.getActivityByDate(this.requestModel)
      .subscribe(pets => {
        this.model = pets;
        console.log(this.model);
      });
  }

  updateDate(value: any): void {
    this.requestModel.date = value;

    this.activityService.getActivityByDate(this.requestModel)
      .subscribe(pets => {
        this.model = pets;
        console.log(this.model);
      });
  }

  add(): void {
    const addActivityModal = this.modalService.open(AddActivityModalComponent, { centered: true });
    addActivityModal.componentInstance.id = this.pet.id;

    this.requestModel.date = this.date;

    addActivityModal.result.then(() => this.activityService.getActivityByDate(this.requestModel)
      .subscribe(pets => {
        this.model = pets;
        console.log(this.model);
      }));
  }
}
