import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserLayoutRoutingModule } from './user-layout-routing.module';
import { AddModalComponent } from './dashboard/add-modal/add-modal.component';
import { NgbModalModule, NgbDatepickerModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { InfoModalComponent } from './dashboard/info-modal/info-modal.component';
import { FormsModule } from '@angular/forms';
import { AddActivityModalComponent } from './dashboard/add-activity-modal/add-activity-modal.component';

@NgModule({
  declarations: [DashboardComponent, AddModalComponent, InfoModalComponent, AddActivityModalComponent],
  imports: [
    CommonModule,
    UserLayoutRoutingModule,
    FormsModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbDropdownModule
  ],
  entryComponents: [AddModalComponent, InfoModalComponent, AddActivityModalComponent]
})
export class UserLayoutModule { }
