import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginModalComponent } from './login-modal/login-modal.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  currentYear: string;

  constructor(private modalService: NgbModal) {
    this.currentYear = new Date().getFullYear().toString();
  }

  ngOnInit() {
  }

  openLoginModal() {
    this.modalService.open(LoginModalComponent, { centered: true });
  }
}
