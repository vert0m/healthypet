import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserLoginModel, User, UserRole } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  model: UserLoginModel;
  error: any;

  constructor(public activeModal: NgbActiveModal,
              private userService: UserService,
              private router: Router) {
    this.model = new UserLoginModel();
  }

  ngOnInit() {
  }

  signIn(): void {
    this.userService.signIn(this.model)
      .subscribe((user: User) => {
        if (user.role === UserRole.Admin) {
          this.router.navigate(['/admin-dashboard']);
        } else {
          this.router.navigate([`/user-dashboard/${user.id}`]);
        }

        this.activeModal.close();
      },
      error => {
        console.error(error);
        this.error = error;
      });
  }

  signUp(): void {
    this.userService.signUp(this.model)
      .subscribe((user: User) => {
        this.router.navigate([`/user-dashboard/${user.id}`]);
        this.activeModal.close();
      },
      error => console.error(error));
  }
}
