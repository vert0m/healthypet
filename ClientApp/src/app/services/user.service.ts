import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserLoginModel, User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public signIn(user: UserLoginModel): Observable<User> {
    return this.http.post<User>(`/api/users/sign-in`, user);
  }

  public signUp(user: UserLoginModel): Observable<User> {
    return this.http.post<User>(`/api/users/sign-up`, user);
  }
}
