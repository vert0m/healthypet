import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pet, PetCreateModel } from '../models/pet';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private http: HttpClient) { }

  public getByUserId(id: string): Observable<Pet[]> {
    return this.http.get<Pet[]>(`api/pets/${id}`);
  }

  public addPet(file: File, model: PetCreateModel): Observable<Pet[]> {
    const formData: FormData = new FormData();

    formData.append('image', file, file.name);
    formData.append('name', model.name);
    formData.append('age', model.age.toString());
    formData.append('userId', model.userId);

    return this.http.post<Pet[]>(`api/pets`, formData);
  }
}
