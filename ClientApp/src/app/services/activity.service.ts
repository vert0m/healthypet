import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivityType, Activity, ActivityByDateModel } from '../models/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient) {}

  public getTypes(): Observable<ActivityType[]> {
    return this.http.get<ActivityType[]>(`api/activities/types`);
  }

  public getByPetId(id: string): Observable<Activity[]> {
    return this.http.get<Activity[]>(`api/activities/${id}`);
  }

  public getActivityByDate(model: ActivityByDateModel): Observable<Activity[]> {
    return this.http.post<Activity[]>(`api/activities/`, model);
  }

  public getForAdmin(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`api/activities/admin`);
  }

  public add(model: Activity, id: string): Observable<Activity[]> {
    return this.http.post<Activity[]>(`api/activities/${id}`, model);
  }
}
