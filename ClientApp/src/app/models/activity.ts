import { Pet } from './pet';

export class ActivityType {
    id: string;
    value: string;
}

export class Activity {
  id: string;
  activityType: ActivityType;
  pet: Pet;
  duration: number;
  startDate: Date;
  description: string;
}

export class ActivityByDateModel {
  petId: string;
  date: Date;
}
