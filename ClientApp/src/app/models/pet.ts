export class Pet {
    id: string;
    name: string;
    age: number;
    pathToImage: string;
}

export class PetCreateModel {
    name: string;
    age: number;
    userId: string;
}
