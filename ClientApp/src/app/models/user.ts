import { Pet } from './pet';

export class UserLoginModel {
    email: string;
    password: string;
}

export class User {
    id: string;
    email: string;
    password: string;
    role: UserRole;
    pets: Pet[];
}

export enum UserRole {
    Admin,
    User
}
