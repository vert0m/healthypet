﻿using Microsoft.AspNetCore.Http;
using System;

namespace HealthyPet.Models
{
    public class PetCreateModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Guid UserId { get; set; }
        public IFormFile Image { get; set; }
    }
}
