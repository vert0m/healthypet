﻿using System;
namespace HealthyPet.Models
{
    public class SignInModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
