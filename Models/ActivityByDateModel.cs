﻿using Microsoft.AspNetCore.Http;
using System;

namespace HealthyPet.Models
{
    public class ActivityByDateModel
    {
        public Guid PetId { get; set; }
        public DateTime Date { get; set; }
    }
}
