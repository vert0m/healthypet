﻿using System;

namespace HealthyPet.Entities
{
    public class PetType
    {
        public Guid Id { get; set; }

        public string Value { get; set; }
    }
}
