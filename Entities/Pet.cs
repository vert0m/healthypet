﻿using System;
using System.Collections.Generic;

namespace HealthyPet.Entities
{
    public class Pet
    {
        public Pet()
        {
            Activity = new List<Activity>();
        }

        public Guid Id { get; set; }

        public PetType PetType { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public string PathToImage { get; set; }

        public List<Activity> Activity { get; set; }
    }
}
