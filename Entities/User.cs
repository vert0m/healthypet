﻿using System;
using System.Collections.Generic;

namespace HealthyPet.Entities
{
    public class User
    {
        public User()
        {
            Role = UserRole.User;
        }

        public Guid Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public UserRole Role { get; set; }

        public List<Pet> Pets { get; set; }
    }
}
