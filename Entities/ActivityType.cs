﻿using System;

namespace HealthyPet.Entities
{
    public class ActivityType
    {
        public Guid Id { get; set; }

        public string Value { get; set; }
    }
}
