﻿using System;

namespace HealthyPet.Entities
{
    public class Activity
    {
        public Guid Id { get; set; }

        public ActivityType ActivityType { get; set; }

        public Pet Pet { get; set; }

        public int Duration { get; set; }

        public DateTime StartDate { get; set; }

        public string Description { get; set; }
    }
}
